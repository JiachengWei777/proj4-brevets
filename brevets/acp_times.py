"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

max_dist = { 200: 34, 400: 32, 600: 30, 1000: 28, 1300: 26 }
min_dist = { 200: 15, 400: 15, 600: 15, 1000: 11.428, 1300: 13.333 }
#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    result = 0
    if control_dist_km != 0:
        latest = 0
        for distance in max_dist:
            if control_dist_km > distance:
                result += (distance - latest)/max_dist[distance]
                latest = distance
            else:
                result += (control_dist_km - latest)/max_dist[distance]
                break
    hour = int(result)
    minute = int((result*60)%60)
    second = int((result*3600)%60)
    shift_minutes = 0
    if second >= 30:
        shift_minutes = 1
    date = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')          #skip seconds
    date = date.shift(hours=+hour, minutes=minute + shift_minutes)
    date = date.replace(tzinfo='US/Pacific')

    return date.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    result = 0
    if control_dist_km != 0:
        latest = 0
        for distance in min_dist:
            if control_dist_km > distance:
                result += (distance - latest) / min_dist[distance]
                latest = distance
            else:
                result += (control_dist_km - latest) / min_dist[distance]
                break
    hour = int(result)
    minute = int((result * 60) % 60)
    second = int((result * 3600) % 60)
    shift_minutes = 0
    if second >= 30:
        shift_minutes = 1
    date = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')  # skip seconds
    date = date.shift(hours=+hour, minutes=minute + shift_minutes)
    date = date.replace(tzinfo='US/Pacific')

    return date.isoformat()
