# Project 4: Brevet time calculator with Ajax
--------------------------
The procedure computes open time and close time by the following table

Control Location(km)  Minimum Speed(km/hr)  Maximum Speed(km/hr)
0 - 200		      15		    34
200 - 400	      15		    32
400 - 600	      15		    40
500 - 1000	      11.428		    28
1000 - 1300	      13.333		    26

--------------------------
Here is one simple example that illustrates how ACP calculator works.

Consider a 200km brevet that finishes at 210km. 
In this brevet, there are some controls before end, 20, 40, and 80.

(OPEN TIME)
While minimum speed is 15km/hr,
20/15 = 1H20MIN
40/15 = 2H40MIN
80/15 = 5H20MIN
210/15 = 14H (end)

(CLOSE TIME)
While maximum speed is 34,
20/34 = 35MIN
40/34 = 1H11MIN
80/34 = 2H21MIN
210/34 = 6H12MIN (end)
 
If controls are 20% longer than brevet distance, there should be a warning raised.

--------------------------
Student: Jiacheng Wei     
Email: jwei@uoregon.edu